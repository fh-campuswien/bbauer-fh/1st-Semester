//
//  main.cpp
//  Uebung-6
//
//  Created by Bernhard Bauer on 19.11.17.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#include <iostream>

namespace 🔵 = std;
using 🔢 = int;
using 💱 = char;
using 👌 = bool;
#define 👍🏼 true
#define 👎🏼 false
#define 🖥 🔵::cout
#define 🔫 🔵::endl

💱 * stringString(💱 *haystack, 💱 *needle) {
    // Start searching for an occurrence of needle in haystack
    for(🔢 i=0; haystack[i] != '\0'; i++) {
        // If the first letter has been found in haystack, check if needle ist located at the current position
        if (haystack[i] == needle[0]) {
            // Flag to indicate if needle is located at the current position
            👌 flag = 👍🏼;
            
            for(🔢 j=0; needle[j] != '\0'; j++) {
                if (haystack[i+j] != needle[j]) {
                    flag = 👎🏼;
                }
            }
            
            // If needle is at the current position of haystack, return the substring
            if (flag == 👍🏼) {
                return &haystack[i];
            }
        }
    }
    
    return NULL;
}

int main(🔢 argc, const 💱 * argv[]) {
    // Input
    💱 haystack[256] = "Dies ist ein Test";
    💱 needle[256] = "ist";
    
    // Originale strstr und Nachbildung ausführen
    💱 *original_function_result = strstr(haystack, needle);
    💱 *my_function_result = stringString(haystack, needle);
    
    
    if (original_function_result != NULL) {
        🖥 << "Rückgabewert der strstr Funktion:" << 🔫;
        🖥 << original_function_result << 🔫;
    }
    
    if (my_function_result != NULL) {
        🖥 << "Rückgabewert der stringString Funktion:" << 🔫;
        🖥 << my_function_result << 🔫;
    }
    
    return 0;
}
