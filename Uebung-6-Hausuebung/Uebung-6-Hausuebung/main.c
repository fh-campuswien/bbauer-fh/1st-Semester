//
//  main.c
//  Uebung-6-Hausuebung
//
//  Created by Bernhard Bauer on 19.12.17.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#include <stdio.h>
#include <string.h>

char data[][100] = {
    "Lorem ipsum dolor sit amet",
    "consetetur sadipscing elitr, sed",
    "diam nonumy eirmod tempor",
    "invidunt ut labore et dolore magna",
    "aliquyam erat, sed diam voluptua.",
    "At vero eos et accusam et justo",
    "duo dolores et ea rebum."
};

void find_data(char search_for[]);

int main(int argc, const char * argv[]) {
    
    char input[100];
    printf("Bitte geben Sie die zu suchende Zeichenkette ein: ");
    if (scanf(" %s", input) != 1) {
        printf("Ungueltige Eingabe!\n");
    } else {
        find_data(input);
    }
    return 0;
}

void find_data(char search_for[]) {
    for (int i=0; i < (sizeof(data) / sizeof(*data)); i++) {
        char *r = strstr(data[i], search_for);
        
        if (r != NULL) {
            printf("found: %s\n", r);
        }
    }
    
}
