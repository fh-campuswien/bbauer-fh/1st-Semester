#include <stdio.h>

int main()
{
	int i, j, num = 0;

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j <=i ; j++)
		{
            num = num + 1;
            
            // Andere Varianten für "num = num + 1;"
//            num++; // Achtung, wenn direkt in printf oder anderen statements
//            num += 1;
            
			printf("%i", num);
		}
		printf("\n");
	}

	printf("\n");
	return 0;
}
