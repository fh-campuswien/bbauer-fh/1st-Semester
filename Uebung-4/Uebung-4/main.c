//
//  main.c
//  Uebung-4
//
//  Created by Bernhard Bauer on 12.12.17.
//  Copyright © 2017 Bernhard Bauer. All rights reserved.
//

#include <stdio.h>
#include "my_math.h"

int main(int argc, const char * argv[]) {
    
    bisection(-1, 5, 0.001);
    
    return 0;
}
