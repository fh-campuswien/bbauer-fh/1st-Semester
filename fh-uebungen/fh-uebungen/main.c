//
//  main.c
//  fh-uebungen
//
//  Created by Bernhard Bauer on 04/11/2017.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#include <stdio.h>

// Hello World ...
void helloWorld() {
    printf("Hello, World!\n");
}

// User Input aus 2 Variable summieren
void sumUserInput() {
    double x;
    double y;
    
    // User Input
    printf("Bitte geben Sie 2 Zahlen ein, welche summiert werden sollen:\n");
    printf("x: ");
    scanf("%lf", &x);
    printf("y: ");
    scanf("%lf", &y);
    
    // Aufsummieren und ausgeben
    printf("x + y = %lf\n", (x+y));
}

// Celsius in grad Fahrenheit umrechnen
void celsiusToFahrenheit() {
    double temperature_celsius;
    
    // User Input
    printf("Bitte geben Sie eine Temperatur in °C ein:\n");
    printf("°C: ");
    scanf("%lf", &temperature_celsius);
    
    // Temperatur umrechnen
    double temperature_fahrenheit = (temperature_celsius * 9 / 5) + 32;
    printf("Temperatur in °F = %lf\n", temperature_fahrenheit);
}

void validate4DigitPostalCode() {
    int postal_code = 0;
    
    // User Input
    printf("Bitte geben Sie eine Postleitzahl ein:\nPLZ:");
    scanf("%i", &postal_code);
    
    // PLZ prüfen
    if (postal_code >= 1000 && postal_code <= 9999) {
        printf("Postleitzahl erfolgreich überprüft.");
    } else {
        printf("Die angegebene Postleitzahl ist ungültig!\n");
        
        // Prüfen, warum die Postleitzahl ungültig ist
        if (postal_code < 1000) {
            printf("Fehler: Postleitzahl zu klein.\n");
        } else if(postal_code > 9999) {
            printf("Fehler: Postleitzahl zu groß.\n");
        }
    }
}

void sumAllUserInput() {
    double sum = 0;
    printf("Bitte geben Sie nacheinander alle Zahlen ein, welche aufsummiert werden sollen:\n");
    
    while (1) {
        double input = 0;
        
        printf("%lf + ", sum);
        if (scanf("%lf", &input) != 1) {
            printf("Die Summe aller Zahlen ist %lf.\n", sum);
            break;
        }
        
        sum += input;
        
    }
}

int main(int argc, const char * argv[]) {
    //    helloWorld();
    //    sumUserInput();
    //    celsiusToFahrenheit();
    //    validate4DigitPostalCode();
    sumAllUserInput();
    
    return 0;
}
