//
//  ConsoleUI.cpp
//  TicTacToe
//
//  Created by Bernhard Bauer on 05/11/2017.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#include "ConsoleUI.hpp"

void ConsoleUI::drawFromArray(Player ***draw, int game_size) {
    // Generate the horitontal line for the given game size
    string horizontal_line = "-";
    for (int i=0; i<game_size; i++) {
        horizontal_line.append("------");
    }
    cout << horizontal_line << endl;
    
    // Generate the main game board for the given game size
    for (int i=0; i<game_size; i++) {
        cout << "|";
        for (int j=0; j<game_size; j++) {
            if (draw[i][j] != NULL) {
                cout << "  " << draw[i][j]->getGameCharacter() << "  |";
            } else {
                cout << "  " << " " << "  |";
            }
        }
        cout << endl;
        cout << horizontal_line << endl;
    }
}


