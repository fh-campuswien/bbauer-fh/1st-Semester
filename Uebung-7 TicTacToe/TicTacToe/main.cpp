//
//  main.cpp
//  TicTacToe
//
//  Created by Bernhard Bauer on 04/11/2017.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include "Game.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
    
    // Create the players
    Player player1("Player 1", false);
    player1.setGameCharacter("x");
    Player player2("Player 2", true);
    player2.setGameCharacter("o");
    
    
    while(true) {
        // Initialize the game
        Game game;
        game.addPlayer(player1);
        game.addPlayer(player2);
        game.loop();
        
        // Display a restart timer
        cout << "Restarting the game: ";
        for (int i=15; i>0; i--) {
            if (i%3 == 0) {
                cout << i/3;
            } else {
                cout << ".";
            }
            usleep(300 * 1000);
        }
        cout << endl;
    }
    
    return 0;
}
