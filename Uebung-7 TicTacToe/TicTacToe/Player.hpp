//
//  Player.hpp
//  TicTacToe
//
//  Created by Bernhard Bauer on 04/11/2017.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#ifndef Player_hpp
#define Player_hpp

#include <iostream>

using namespace std;

class Player {
    
    private:
        string name = "Player";
        bool is_ai = false;
        string character = "?";
        // Sanitizes user / ai input
        int sanitizeInput(int in);
    
    public:
        Player(string name, bool is_ai);
        // Retreive the player name
        string getName();
        // Set the game character which will be used for the player
        void setGameCharacter(string character);
        // Retreive the game character for the player
        string getGameCharacter();
        // The next move (input from a human being) -> number between 0 and 8
        int getUserInput();
        // Check if the player is an AI
        bool isAI();
    
};

#endif /* Player_hpp */
