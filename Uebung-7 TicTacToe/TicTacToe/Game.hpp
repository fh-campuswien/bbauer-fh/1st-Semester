//
//  Game.hpp
//  TicTacToe
//
//  Created by Bernhard Bauer on 04/11/2017.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp

#include "Player.hpp"
#include "ConsoleUI.hpp"
#include <iostream>

using namespace std;

class Game {
    
    private:
    
        const int game_size = 3;
        const int line_length_to_win = 3;
        Player ***game_board = { NULL };
        Player *players[2] = { NULL };
        ConsoleUI console_ui;
    
        int move_count = 0;
        bool is_finished = false;
        Player *winner = NULL;
    
        Player *move(int x, int y, Player *player);
        Player *simulateMove(int x, int y, Player *player);
        int checkIfPlayerCouldWin(Player *player); // Check if it is possible for a player to win with the next move
    
    public:
        Game();
        ~Game();
        // Adds a player to the game -> maximum number of players supported is 2
        bool addPlayer(Player player);
        bool isFinished();
        int loop();
    
};

#endif /* Game_hpp */
